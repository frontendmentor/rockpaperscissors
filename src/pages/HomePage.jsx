import GameBoard from "../components/GameBoard"

const HomePage = () => {
    return (
        <GameBoard />
    )
}

export default HomePage