import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'

import '../styles/css/Result.css'
import { useLocation } from 'react-router-dom';
import Piece from '../components/Piece';
import PieceTypes from '../PieceTypes.json'
import useLocalStorage from '../hooks/useLocalStorage';

const ResultPage = ({ updateScore }) => {

    const [itemTypeSelected, setItemTypeSelected] = useState(null)
    const [isWin, setIsWin] = useState(false)
    const [score, setScore] = useLocalStorage("score")

    const search = useLocation().search
    const type = new URLSearchParams(search).get("type")

    useEffect(() => {
        const timer = setTimeout(() => {
            const itemTypes = PieceTypes.filter((s) => s.type != type)
            const rand = Math.floor(Math.random() * itemTypes.length)
            setItemTypeSelected(itemTypes[rand].type)
        }, 1500)

        return () => {
            clearInterval(timer)
        }
    }, [])

    useEffect(() => {
        if (type === "paper" && itemTypeSelected === "rock") {
            setIsWin(true)
        }

        if (type === "scissors" && itemTypeSelected === "paper") {
            setIsWin(true)
        }

        if (type === "rock" && itemTypeSelected === "scissors") {
            setIsWin(true)
        }
    }, [itemTypeSelected])

    useEffect(() => {
        if (isWin) {
            updateScore(score + 1)
        }
    }, [isWin])

    return (
        <div id="wrapper_result">

            <div id="you">
                <span id="title">YOU PICKED</span>
                <Piece type={type} isWin={isWin} link={false} position={false} width="250px" height="250px" />
            </div>

            {itemTypeSelected != null && (
                <div id="restart">
                    <span id="title">{isWin ? "YOU WIN" : "YOU LOSE"}</span>
                    <Link to="/">
                        <motion.button
                            className={isWin ? "win_button" : "lose_button"}
                            whileHover={{ scale: 1.1 }}
                            whileTap={{ scale: 0.9 }}
                        >
                            PLAY AGAIN
                        </motion.button>
                    </Link>
                </div>
            )}

            <div id="him">
                <span id="title">THE HOUSE PICKED</span>
                {itemTypeSelected != null ? (
                    <Piece type={itemTypeSelected} isWin={!isWin} link={false} position={false} width="250px" height="250px" />
                ) : (
                    <div id="piece_location"></div>
                )}
            </div>

        </div>
    )
}

export default ResultPage