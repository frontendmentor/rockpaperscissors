import Menu from '../components/Menu'
import Modal from '../components/Modal'
import { useEffect, useState } from 'react'
import { AnimatePresence, motion } from 'framer-motion'

const MainLayout = (props) => {

    const [modalIsShowing, setModalIsShowing] = useState(false)

    const openModal = () => {
        setModalIsShowing(true)
    }

    const closeModal = () => {
        setModalIsShowing(false)
    }

    useEffect(() => {
        if (modalIsShowing) {
            document.querySelector('body').style.overflow = "hidden"
        } else {
            document.querySelector('body').style.overflow = "visible"
        }
    }, [modalIsShowing])

    return (
        <>
            <Menu score={props.score} />

            {props.children}

            <footer id="footer" role="contentinfo">

                <div id="credits">
                    <span>Challenge by <a href="https://www.frontendmentor.io?ref=challenge" rel="noreferrer" target="_blank">Frontend Mentor</a></span>
                    <span> | Coded by <a href="http://valentin-laidet.fr" target="_blank">Valentin Laidet</a></span>
                </div>

                <motion.button
                    id="rules_button"
                    whileHover={{ scale: 1.1, cursor: 'pointer' }}
                    whileTap={{ scale: 0.9 }}
                    onClick={() => (modalIsShowing ? closeModal() : openModal())}
                >
                    RULES
                </motion.button>
            </footer>

            <AnimatePresence
                initial={false}
                exitBeforeEnter={true}
                onExitComplete={() => null}
            >
                {modalIsShowing && <Modal handleClose={closeModal} />}
            </AnimatePresence>
        </>
    )
}

export default MainLayout