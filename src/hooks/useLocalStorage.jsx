import { useEffect, useState } from 'react'

const getValueFromStorage = key => JSON.parse(localStorage.getItem(key))
const setValueInStorage = (key, value) => localStorage.setItem(key, JSON.stringify(value))


const useLocalStorage = (storageKey) => {

    console.log("get value from storage : " + getValueFromStorage(storageKey));
    const [data, setData] = useState(getValueFromStorage(storageKey) != null ? getValueFromStorage(storageKey) : 0)

    useEffect(() => {
        console.log("set value in storage : " + data);
        setValueInStorage(storageKey, data)
    }, [data])

    return [data, setData]
}

export default useLocalStorage