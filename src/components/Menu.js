import React from 'react'
import '../styles/css/Menu.css'
import logo from '../images/logo.svg'

const Menu = ({ score }) => {

    return (
        <header id='menu' role="banner">
            <img src={logo} alt="" id="logo" />

            <div id="score">
                <p>SCORE</p>
                <p>{score}</p>
            </div>
        </header>
    )
}

export default Menu