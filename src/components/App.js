import AnimatedRoutes from "./AnimatedRoutes";
import useLocalStorage from "../hooks/useLocalStorage"
import '../styles/css/App.css'

const App = () => {

  const [score, setScore] = useLocalStorage("score")

  const updateScore = newScore => {
    setScore(newScore)
  }

  return (
    <div id="App">
      <AnimatedRoutes score={score} updateScore={updateScore} />
    </div>
  );
}

export default App;
