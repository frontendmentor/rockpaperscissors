import React from 'react'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'
import '../styles/css/Piece.css'

// Images
import PaperImage from '../images/icon-paper.svg'
import ScissorsImage from '../images/icon-scissors.svg'
import RockImage from '../images/icon-rock.svg'


const Piece = ({ type, isWin, link, position, width, height }) => {

  const getImage = type => {
    switch (type) {
      case "paper":
        return <img src={PaperImage} className='piece-image' alt="paper" />
      case "scissors":
        return <img src={ScissorsImage} className='piece-image' alt="scissors" />
      case "rock":
        return <img src={RockImage} className='piece-image' alt="rock" />
      default:
        return null
    }
  }

  return link ? (
    <Link to={"/result?type=" + type} data-type={type} data-position={position}>
      <div className="piece" style={{ width: width, height: height }}>
        <motion.button
          className={isWin && "win"}
          whileHover={{ scale: 1.1, cursor: 'pointer' }}
          whileTap={{ scale: 1 }}
          style={{ width: width, height: height }}
        >
          <span style={{ width: width, height: height }}>
            {getImage(type)}
          </span>
        </motion.button>
      </div>
    </Link >
  ) : (
    <div className="piece" data-type={type} data-position={position} style={{ width: width, height: height }}>
      <button className={isWin && "win"} style={{ width: width, height: height }}>
        <span style={{ width: 'calc(' + width + '/ 1.33)', height: 'calc(' + height + '/ 1.33)' }}>
          {getImage(type)}
        </span>
      </button>
    </div>
  )
}

export default Piece