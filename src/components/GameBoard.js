import { motion } from 'framer-motion'
import '../styles/css/Gameboard.css'
import PieceTypes from '../PieceTypes.json'
import Piece from './Piece'

const GameBoard = () => {

    const dropDown = {
        hidden: {
            y: "100vh",
            opacity: 0,
        },
        visible: {
            y: "0",
            opacity: 1,
            transition: {
                duration: 0.5,
                type: "spring",
                damping: 50,
                stiffness: 300
            }
        },
        exit: {
            y: "-100vh",
            opacity: 0,
        },
    }

    return (
        <motion.div
            id="wrapper_gameboard"
            variants={dropDown}
            initial="hidden"
            animate="visible"
            exit="exit"
            role="main"
        >
            {PieceTypes.map(item => {
                return (
                    <Piece key={item.id} type={item.type} link={true} position={true} />
                )
            })}
        </motion.div>
    )
}

export default GameBoard