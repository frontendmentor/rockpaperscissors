import React from 'react'
import { motion } from 'framer-motion'
import '../styles/css/Modal.css'

import ExitIcon from '../images/icon-close.svg'
import RulesImage from '../images/image-rules.svg'

const dropIn = {
    hidden: {
        y: "-100vh",
        opacity: 0,
    },
    visible: {
        y: "0",
        opacity: 1,
        transition: {
            duration: 0.3,
            type: "spring",
            damping: 25,
            stiffness: 500
        }
    },
    exit: {
        y: "100vh",
        opacity: 0,
    },
}

const Modal = ({ handleClose }) => {

    return (
        <motion.div
            id="modal"
            onClick={handleClose}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
        >

            <motion.div
                id="modal-content"
                onClick={(e) => e.stopPropagation()}
                variants={dropIn}
                initial="hidden"
                animate="visible"
                exit="exit"
            >

                <div id="modal-header">
                    <h4 id="modal-title">RULES</h4>
                    <img src={ExitIcon} id="exit" onClick={handleClose} />
                </div>

                <div id="modal-body">
                    <img src={RulesImage} alt="rules" id="image" />
                </div>

            </motion.div>

        </motion.div>
    )
}

export default Modal