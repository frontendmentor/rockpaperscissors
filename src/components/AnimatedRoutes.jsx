import { Routes, Route, Navigate, useLocation } from 'react-router-dom'
import { AnimatePresence } from "framer-motion"
import MainLayout from '../layouts/MainLayout'
import HomePage from '../pages/HomePage'
import ResultPage from '../pages/ResultPage'

const AnimatedRoutes = ({ score, updateScore }) => {

    const location = useLocation();

    return (
        <AnimatePresence>
            <MainLayout score={score}>
                <Routes location={location} key={location.pathname}>
                    <Route path="/" element={<HomePage />} />
                    <Route path="/result" element={<ResultPage updateScore={updateScore} />} />
                    <Route
                        path="*"
                        element={<Navigate to="/" replace />}
                    />
                </Routes>
            </MainLayout>
        </AnimatePresence>
    )
}

export default AnimatedRoutes